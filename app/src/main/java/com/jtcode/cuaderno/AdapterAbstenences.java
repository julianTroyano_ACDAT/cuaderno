package com.jtcode.cuaderno;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jtcode.cuaderno.POJO.Abstenence;
import com.jtcode.cuaderno.POJO.Student;

import java.util.ArrayList;
import java.util.List;


public class AdapterAbstenences extends ArrayAdapter<Abstenence>{
    private List<Abstenence> abstenenceList ;

    public AdapterAbstenences(Context context, List<Abstenence> resource) {
        super(context, R.layout.item_abstenences, resource);
        this.abstenenceList=resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootview=convertView;
        Holder hol;

        if(rootview==null){
            hol= new Holder();
            rootview= LayoutInflater.from(getContext()).inflate(R.layout.item_abstenences,null);
            hol.namestu=(TextView)rootview.findViewById(R.id.ITEMNameABST);
            hol.trabajo=(TextView)rootview.findViewById(R.id.ITEMtrabajo);
            hol.actitud=(TextView)rootview.findViewById(R.id.ITEMActitud);
            hol.falta=(TextView)rootview.findViewById(R.id.ITEMfalta);

            rootview.setTag(hol);
        }else{
            hol=(Holder)rootview.getTag();
        }

        hol.namestu.setText(getItem(position).getName());
        hol.falta.setText(getItem(position).getAbstenence());
        hol.actitud.setText(getItem(position).getAptitude());
        hol.trabajo.setText(getItem(position).getWork());
        return rootview;
    }

    public void set(ArrayList<Abstenence> abstenenceList) {
        //set all the sites
        this.abstenenceList.clear();
        this.abstenenceList.addAll(abstenenceList);
        notifyDataSetChanged();
    }

    public Abstenence getAt(int position){
        //get the site in the position
        return  this.abstenenceList.get(position);
    }

    public void add(Abstenence site) {
        //add a site
        this.abstenenceList.add(site);
        notifyDataSetChanged();
        //notifyItemChanged(sitesList.size()-1);
        //notifyItemRangeChanged(0,sitesList.size()-1);
    }

    public void modifyAt(Abstenence abstenence, int position) {
        //modify a site in the position
        this.abstenenceList.set(position,abstenence);
        notifyDataSetChanged();

    }

    public void removeAt(int position) {
        //delete a site in the position
        this.abstenenceList.remove(position);
        notifyDataSetChanged();
    }
    public void clear(){
        this.abstenenceList.clear();
    }


    class Holder {
        TextView namestu,trabajo,falta,actitud;
    }
}
