package com.jtcode.cuaderno.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jtcode.cuaderno.AdapterAbstenences;
import com.jtcode.cuaderno.POJO.Abstenence;
import com.jtcode.cuaderno.R;

import java.util.ArrayList;


public class AbsencesFragments extends Fragment {

    private static AbsencesFragments absencesFragments;
    private static AbstenenceIterationlistener mListener;
    private static AdapterAbstenences adapterR;
    private static ListView listView;

    public AbsencesFragments() {}

    public static Fragment newInstance(){
        if(absencesFragments==null) {
            absencesFragments= new AbsencesFragments();
        }
        return absencesFragments;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_absences,container,false);
        listView=(ListView) rootView.findViewById(R.id.listaFaltas);
        listView.setAdapter(adapterR);
        registerForContextMenu(listView);
        return rootView;
    }


    public static void setAdapter(AdapterAbstenences adapter){
        adapterR=adapter;
        listView.setAdapter(adapterR);
    }

    public static AdapterAbstenences getAdapter(){
        return adapterR;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // listView.setAdapter(adapterR);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mListener.editAbstenence(adapterR.getAt(position));
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AbstenenceIterationlistener) {
            mListener = (AbstenenceIterationlistener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement the AbstenenceIteratorListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface AbstenenceIterationlistener {
       void editAbstenence(Abstenence abstenence);
    }

}
