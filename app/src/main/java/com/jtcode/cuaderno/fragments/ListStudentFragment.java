package com.jtcode.cuaderno.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jtcode.cuaderno.Adapter;
import com.jtcode.cuaderno.R;
import com.jtcode.cuaderno.POJO.Student;

public class ListStudentFragment extends Fragment {

    private StudentIterationListener mListener;
    private static ListStudentFragment listStudentFragment;
    private static Adapter adapterR;
    private static ListView listView;

    public ListStudentFragment() {}

    public static Fragment newInstance(){
        if(listStudentFragment==null) {
            listStudentFragment= new ListStudentFragment();
        }
        return listStudentFragment;
    }
    public static void setAdapter(Adapter adapter){
        adapterR=adapter;
        listView.setAdapter(adapterR);
    }

    public static Adapter getAdapter(){
        return adapterR;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //adapterR= new Adapter(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_list,container,false);
        listView=(ListView) rootView.findViewById(R.id.lista);
        listView.setAdapter(adapterR);
        registerForContextMenu(listView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // listView.setAdapter(adapterR);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mListener.editStudent(adapterR.getAt(position));
            }
        });
        listView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //show the option, delete or send a mail to student
                return false;
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.menu,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo());
        Student stem = adapterR.getItem(info.position);

        switch (item.getItemId()) {
            case R.id.action_delete:
                mListener.delete(stem);
                break;
            case R.id.action_email:
                mListener.sendEmail();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StudentIterationListener) {
            mListener = (StudentIterationListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement the StudentIterationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface StudentIterationListener {
        void editStudent(Student student);
        void delete(Student student);
        void sendEmail();
    }
}
