package com.jtcode.cuaderno;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jtcode.cuaderno.POJO.Abstenence;
import com.jtcode.cuaderno.POJO.Student;
import com.jtcode.cuaderno.fragments.AbsencesFragments;
import com.jtcode.cuaderno.fragments.ListStudentFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class HomeActivity extends AppCompatActivity implements ListStudentFragment.StudentIterationListener,AbsencesFragments.AbstenenceIterationlistener{

    ProgressDialog progressDialog;
    ViewPager viewPager;
    TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;
    FloatingActionButton fab;
    Adapter adapterStudent;
    AdapterAbstenences adapterAbstenences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //tabs for the view pager
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(ListStudentFragment.newInstance(), "Lista");
        viewPagerAdapter.addFragment(AbsencesFragments.newInstance(), "Faltas");
        viewPager.setAdapter(viewPagerAdapter);

        viewPager.setPageTransformer(true, new AnimationSwipe());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changeBtn(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);

        //click on the fab
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnOptions();
            }
        });

        getAllStudent();
        getAllAbstenences();
    }

    private void changeBtn(int pos) {
        switch (pos) {
            case 0:
                fab.setImageResource(R.drawable.ic_add_user);
                break;
            case 1:
                fab.setImageResource(R.drawable.ic_send);
                break;
        }
        AnimationSwipe.rotateAnimation(fab);
    }

    private void btnOptions() {
        switch (viewPager.getCurrentItem()) {
            case 0://list
                addStudent();
                break;

            case 1: //abteneces
                addAbstenence();
                break;
        }
    }

    private void addAbstenence(){
        abstenenceManager(null);
    }
    private void abstenenceManager(final Abstenence abstenence){
        //edit or add in a alertDialog

        final String faltas[] = {"Justificada","Injustificada","Retraso"};
        final String trabajos[] = {"Bueno","Regular","Malo"};
        final String actitudes[] = {"Negativa","Posititva"};
        final String names[]=adapterStudent.getNames();
        final String ids[]=adapterStudent.getid();


        ArrayAdapter<String> sptrabajo= new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,trabajos);
        ArrayAdapter<String> spactyitud= new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,actitudes);
        ArrayAdapter<String> spfalta= new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,faltas);
        ArrayAdapter<String> spnames= new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,names);

        //---------------------------------//
        View view = LayoutInflater.from(HomeActivity.this).inflate(R.layout.layout_abstenence, null);
        final Spinner student = (Spinner) view.findViewById(R.id.spstudentL);
        final Spinner  falta = (Spinner) view.findViewById(R.id.spFaltas);
        final Spinner trabajo = (Spinner) view.findViewById(R.id.sptrabajo);
        final Spinner actitud = (Spinner) view.findViewById(R.id.spactitud);
        final EditText observaciones = (EditText) view.findViewById(R.id.edtobservaciones);

        falta.setAdapter(spfalta);
        trabajo.setAdapter(sptrabajo);
        actitud.setAdapter(spactyitud);
        student.setAdapter(spnames);

        if(abstenence!=null) {
            student.setSelection(spnames.getPosition(abstenence.getName()));
            falta.setSelection(spfalta.getPosition(abstenence.getAbstenence()));
            trabajo.setSelection(sptrabajo.getPosition(abstenence.getWork()));
            actitud.setSelection(spactyitud.getPosition(abstenence.getAptitude()));
            observaciones.setText(abstenence.getDescription());
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        dialog.setView(view);
        dialog.setTitle("Falta");
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Abstenence stemp= new Abstenence();
                stemp.setStudent (names[student.getSelectedItemPosition()]);
                stemp.setId(ids[student.getSelectedItemPosition()]);
                stemp.setWork(trabajos[trabajo.getSelectedItemPosition()]);
                stemp.setAbstenence(faltas[falta.getSelectedItemPosition()]);
                stemp.setAptitude(actitudes[actitud.getSelectedItemPosition()]);
                stemp.setDescription(observaciones.getText().toString());

                if(abstenence!=null) {
                    stemp.setId(stemp.getId());
                    updateAbstenences(abstenence);
                }
                else
                    insertAbstenences(stemp);

            }

        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    }

    private void addStudent(){
        studentManage(null);
    }

    @Override
    public void editStudent(Student student) {
        studentManage(student);
    }

    private void studentManage(final Student student){
        //edit or add in a alertDialog

        //---------------------------------//
       View view = LayoutInflater.from(HomeActivity.this).inflate(R.layout.student_dialog, null);
        //<!-- apellidos, nombre, dirección, ciudad, código postal, teléfono y email -->
        final EditText name = (EditText) view.findViewById(R.id.edtNewName);
        final EditText lastName = (EditText) view.findViewById(R.id.edtNewLastName);
        final EditText adress = (EditText) view.findViewById(R.id.edtNewAdress);
        final EditText city = (EditText) view.findViewById(R.id.edtNewCity);
        final EditText cp = (EditText) view.findViewById(R.id.edtNewCP);
        final EditText phone=(EditText)view.findViewById(R.id.edtNewPhone);
        final EditText email=(EditText)view.findViewById(R.id.edtNewEmail);

        if(student!=null){
            name.setText(student.getName());
            lastName.setText(student.getLastname());
            adress.setText(student.getAdress());
            city.setText(student.getCity());
            cp.setText(student.getCp());
            phone.setText(student.getPhone());
            email.setText(student.getEmail());
        }else{
            name.setText("Nombre");
            lastName.setText("Apellido");
            adress.setText("direccion");
            city.setText("ciudad");
            cp.setText("CP");
            phone.setText("Telefono");
            email.setText("email");
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        dialog.setView(view);
        dialog.setTitle("Alumno");
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Student stemp= new Student();
                stemp.setName(name.getText().toString());
                stemp.setLastname(lastName.getText().toString());
                stemp.setAdress(adress.getText().toString());
                stemp.setCity(city.getText().toString());
                stemp.setCp(cp.getText().toString());
                stemp.setEmail(email.getText().toString());
                stemp.setPhone(phone.getText().toString());

                if(student!=null) {
                    stemp.setId(student.getId());
                    updateStudent(stemp);
                }
                else
                 insertStudent(stemp);

            }

        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    }

    @Override
    public void delete(final Student student) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(student.getName() + "\n¿Do you want to delete?")
                .setTitle("Delete")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        deleteStudent(student.getId());
                        adapterStudent.remove(student);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    @Override
    public void sendEmail() {
        View view = LayoutInflater.from(HomeActivity.this).inflate(R.layout.email_dialog, null);
        final EditText from = (EditText) view.findViewById(R.id.edtFrom);
        final EditText to = (EditText) view.findViewById(R.id.edtTo);
        final EditText pwd = (EditText) view.findViewById(R.id.edtPwd);
        final EditText subject = (EditText) view.findViewById(R.id.edtSubject);
        final EditText body = (EditText) view.findViewById(R.id.edtBody);

        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        dialog.setView(view);
        dialog.setTitle("ENVIO DE EMAIL");
        dialog.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sendMsg(to.getText().toString(), subject.getText().toString(), body.getText().toString(),
                        from.getText().toString(), pwd.getText().toString());
            }

        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();

    }


    //region Student manage
    //students
    private void getAllStudent() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();
        progressDialog.setMessage("Descargando...");
        progressDialog.show();

        client.get(MyConstants.URL+MyConstants.ALL_STUDENTS, new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, responseString, Toast.LENGTH_LONG).show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progressDialog.dismiss();
                Gson gson = new Gson();
                try {
                    Result result = gson.fromJson(String.valueOf(response), Result.class);
                    if(result.getCode()){
                        if(adapterStudent ==null) {
                            adapterStudent = new Adapter(HomeActivity.this, result.getStudents());
                        }else{
                         //   adapterStudent.clear();
                            adapterStudent.set(result.getStudents());
                        }
                        ListStudentFragment.setAdapter(adapterStudent);
                    }else {
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JsonSyntaxException e) {
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    private void insertStudent(Student student){

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();
        params.put(MyConstants.NAME, student.getName());
        params.put(MyConstants.LASTNAME, student.getLastname());
        params.put(MyConstants.EMAIL, student.getEmail());
        params.put(MyConstants.ADRESS,student.getAdress());
        params.put(MyConstants.CITY,student.getCity());
        params.put(MyConstants.CP,student.getCp());
        params.put(MyConstants.TELEPFONE,student.getPhone());
        progressDialog.setMessage("Guardando...");
        progressDialog.show();
        client.post(MyConstants.URL + MyConstants.CRUD_ACTIONS, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.dismiss();
                String response = new String(responseBody);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Result result = gson.fromJson(String.valueOf(jsonObject), Result.class);
                    if(result.getCode()){
                       // adapterStudent.set(result.getStudents());
                        getAllStudent();
                    }else {
                        Log.e("error",result.getMessage());
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("error",e.getMessage());
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Log.e("error",error.getMessage());
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void deleteStudent(String id){

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();
        params.put(MyConstants.ID,id);
        progressDialog.setMessage("Borrando...");
        progressDialog.show();
        Log.e("ruta",MyConstants.URL + MyConstants.CRUD_ACTIONS + "/" +id );
        client.delete(MyConstants.URL + MyConstants.CRUD_ACTIONS + "/" +id , params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.dismiss();
                String response = new String(responseBody);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Result result = gson.fromJson(String.valueOf(jsonObject), Result.class);

                    if(result.getCode()){
                        getAllStudent();
                    }else {
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateStudent(Student student){

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();
        params.put(MyConstants.ID,student.getId());
        params.put(MyConstants.NAME, student.getName());
        params.put(MyConstants.LASTNAME, student.getLastname());
        params.put(MyConstants.EMAIL, student.getEmail());
        params.put(MyConstants.ADRESS,student.getAdress());
        params.put(MyConstants.CITY,student.getCity());
        params.put(MyConstants.CP,student.getCp());
        params.put(MyConstants.TELEPFONE,student.getPhone());

        progressDialog.setMessage("Actualizando...");
        progressDialog.show();

        client.put(MyConstants.URL + MyConstants.CRUD_ACTIONS + "/"+student.getId(), params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                progressDialog.dismiss();
                String response = new String(responseBody);
                Gson gson = new Gson();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Result result = gson.fromJson(String.valueOf(jsonObject), Result.class);
                    if(result.getCode()){
                        getAllStudent();
                    }else {
                        Log.e("error",result.getMessage());
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("error",e.getMessage());
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Log.e("error",error.getMessage());
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    //endregion

//region faltas
    private void getAllAbstenences() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        progressDialog.setMessage("Descargando...");
        progressDialog.show();
        client.get(MyConstants.URL+MyConstants.ALL_ABSTENENCES, new JsonHttpResponseHandler(){
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, responseString, Toast.LENGTH_LONG).show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progressDialog.dismiss();
                Gson gson = new Gson();
                try {
                    ResultAbstenences result = gson.fromJson(String.valueOf(response), ResultAbstenences.class);
                    if(result.getCode()){
                        if(adapterAbstenences ==null) {
                            adapterAbstenences = new AdapterAbstenences(HomeActivity.this, result.getAbstenences());
                            AbsencesFragments.setAdapter(adapterAbstenences);
                        }else{
                            adapterAbstenences.set(result.getAbstenences());
                        }
                        //AbsencesFragments.setAdapter(adapterAbstenences);
                    }else {
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JsonSyntaxException e) {
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void insertAbstenences(Abstenence abstenence){

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();
        params.put(MyConstants.A_STUDENT,abstenence.getId());
        params.put(MyConstants.A_ABSTENENCE,abstenence.getAbstenence());
        params.put(MyConstants.A_WORK,abstenence.getWork());
        params.put(MyConstants.A_APTITUDE,abstenence.getAptitude());
        params.put(MyConstants.A_DESCRIPTION,abstenence.getDescription());
        progressDialog.setMessage("Guardando...");
        progressDialog.show();
        client.post(MyConstants.URL + MyConstants.A_CRUD_ACTIONS, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.dismiss();
                String response = new String(responseBody);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    ResultAbstenences result = gson.fromJson(String.valueOf(jsonObject), ResultAbstenences.class);
                    if(result.getCode()){
                        adapterAbstenences.set(result.getAbstenences());
                        getAllAbstenences();
                    }else {
                        Log.e("error",result.getMessage());
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("error",e.getMessage());
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Log.e("error",error.getMessage());
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

   private void updateAbstenences(Abstenence abstenence){

       AsyncHttpClient client = new AsyncHttpClient();
       client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
       RequestParams params = new RequestParams();
       params.put(MyConstants.A_STUDENT,abstenence.getId());
       params.put(MyConstants.A_ABSTENENCE,abstenence.getAbstenence());
       params.put(MyConstants.A_WORK,abstenence.getWork());
       params.put(MyConstants.A_APTITUDE,abstenence.getAptitude());
       params.put(MyConstants.A_DESCRIPTION,abstenence.getDescription());
       params.put(MyConstants.A_ID,abstenence.getId());

        progressDialog.setMessage("Actualizando...");
        progressDialog.show();

        client.put(MyConstants.URL + MyConstants.A_CRUD_ACTIONS + "/"+abstenence.getId(), params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                progressDialog.dismiss();
                String response = new String(responseBody);
                Gson gson = new Gson();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Result result = gson.fromJson(String.valueOf(jsonObject), Result.class);
                    if(result.getCode()){
                        getAllAbstenences();
                    }else {
                        Log.e("error",result.getMessage());
                        Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("error",e.getMessage());
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Log.e("error",error.getMessage());
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    //endregion


    @Override
    public void editAbstenence(Abstenence abstenence) {
       abstenenceManager(abstenence);
        //updateAbstenences(abstenence);
    }

    private void sendMsg(String to, String subject, String msg, String from, String pwd){

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();
        params.put(MyConstants.TO, to );
        params.put(MyConstants.SUBJECT, subject);
        params.put(MyConstants.MSG, msg);
        params.put(MyConstants.FROM, from);
        params.put(MyConstants.PWD, pwd);
        progressDialog.setMessage("Enviando...");
        progressDialog.show();

        client.post(MyConstants.URL + MyConstants.MAIL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.dismiss();
                String response = new String(responseBody);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Result result = gson.fromJson(String.valueOf(jsonObject), Result.class);
                    Toast.makeText(HomeActivity.this, result.getMessage(), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

//the adapterStudent for the fagments

class ViewPagerAdapter extends FragmentPagerAdapter{
    private ArrayList<Fragment> fragmentArrayList= new ArrayList<>();
    private ArrayList<String> fragmentName= new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }
    public void addFragment(Fragment f,String s){
        fragmentArrayList.add(f);
        fragmentName.add(s);
    }
    public CharSequence getPageTitle(int pos){
        return fragmentName.get(pos);
    }
}
