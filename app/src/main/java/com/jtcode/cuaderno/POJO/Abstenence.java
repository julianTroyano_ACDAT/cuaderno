package com.jtcode.cuaderno.POJO;

import java.io.Serializable;
import java.util.Date;

public class Abstenence implements Serializable{

    String id;
    String name;
    String student;
    Date date;
    String abstenence;
    String work;
    String aptitude;
    String description;

    public Abstenence() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() { return id;  }

    public void setId(String id) { this.id = id;   }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAptitude() {
        return aptitude;
    }

    public void setAptitude(String aptitude) {
        this.aptitude = aptitude;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getAbstenence() {
        return abstenence;
    }

    public void setAbstenence(String abstenence) {
        this.abstenence = abstenence;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }
}
