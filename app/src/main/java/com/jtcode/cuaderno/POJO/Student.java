package com.jtcode.cuaderno.POJO;

import java.io.Serializable;

public class Student implements Serializable{
//apellidos, nombre, dirección, ciudad, código postal, teléfono y email
    String id;
    String name;
    String lastname;
    String adress;
    String city;
    String cp;
    String phone;
    String email;


    public Student(){}

    public void setId(String id) {
        this.id = id;
    }
    public String getId() { return id;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
