package com.jtcode.cuaderno;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.jtcode.cuaderno.POJO.Student;
import java.util.ArrayList;
import java.util.List;


public class Adapter extends ArrayAdapter<Student> {

    private List<Student> studentList;

    public Adapter(Context context,List<Student> resource) {
        super(context, R.layout.item_student, resource);
        this.studentList=resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootview=convertView;
        Holder hol;

        if(rootview==null){
            hol= new Holder();
            rootview= LayoutInflater.from(getContext()).inflate(R.layout.item_student,null);
            hol.name=(TextView)rootview.findViewById(R.id.name);
            hol.lastname=(TextView)rootview.findViewById(R.id.lastname);
            hol.adress=(TextView)rootview.findViewById(R.id.adress);
            hol.city=(TextView)rootview.findViewById(R.id.city);
            hol.cp=(TextView)rootview.findViewById(R.id.cp);
            hol.email=(TextView)rootview.findViewById(R.id.email);
            hol.telephone=(TextView)rootview.findViewById(R.id.tlf);
            rootview.setTag(hol);
        }else{
            hol=(Holder)rootview.getTag();
        }

        hol.name.setText(getItem(position).getName());
        hol.lastname.setText(getItem(position).getLastname());
        hol.adress.setText(getItem(position).getAdress());
        hol.city.setText(getItem(position).getCity());
        hol.cp.setText(getItem(position).getCp());
        hol.email.setText(getItem(position).getEmail());
        hol.telephone.setText(getItem(position).getPhone());

        return rootview;
    }

    public void set(ArrayList<Student> studentList) {
        //set all the sites
        this.studentList.clear();
        this.studentList.addAll(studentList);
        this.notifyDataSetChanged();
        notifyDataSetChanged();
    }

    public Student getAt(int position){
        //get the site in the position
        return  this.studentList.get(position);
    }

    public void add(Student site) {
        //add a site
        this.studentList.add(site);
        notifyDataSetChanged();
        //notifyItemChanged(sitesList.size()-1);
        //notifyItemRangeChanged(0,sitesList.size()-1);
    }

    public void modifyAt(Student student, int position) {
        //modify a site in the position
        this.studentList.set(position,student);
        notifyDataSetChanged();

    }

    public void removeAt(int position) {
        //delete a site in the position
        this.studentList.remove(position);
        notifyDataSetChanged();
    }
    public void clear(){
       this.studentList.clear();
    }

    public String[] getNames(){
        String[] names=new String[studentList.size()];
        for (int i=0;i<studentList.size();i++) {
            names[i]= studentList.get(i).getName();
        }
        return names;
    }

    public String[] getid(){
        String[] id=new String[studentList.size()];
        for (int i=0;i<studentList.size();i++) {
            id[i]= studentList.get(i).getId();
        }
        return id;
    }

    class Holder {
        TextView name, lastname, adress, city,cp,telephone,email;
    }
}
