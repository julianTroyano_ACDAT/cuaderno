package com.jtcode.cuaderno;

import java.util.Date;

public interface MyConstants {
      String URL = "https://julian.alumno.club/";
    //  String URL="http://192.168.0.108:8080/";
      String ALL_STUDENTS="students";
      String ALL_ABSTENENCES="abstenences";

      String CRUD_ACTIONS = "student";
      String A_CRUD_ACTIONS = "abstenence";
      String MAIL = "mail";

      String ID = "id";
    //user const
      String EMAIL = "email";
      String NAME = "name";
      String LASTNAME="lastname";
      String ADRESS="adress";
      String CITY="city";
      String CP="cp";
      String TELEPFONE="telephone";

    //email
      String PWD = "pwd";
      String MSG = "msg";
      String FROM = "from";
      String TO = "to";
      String SUBJECT = "subject";


    //control
    String A_ID="id";
    String A_STUDENT="student";
    String A_DATE="date";
    String A_ABSTENENCE="abstenence";
    String A_WORK="work";
    String A_APTITUDE="aptitude";
    String A_DESCRIPTION="description";

}
