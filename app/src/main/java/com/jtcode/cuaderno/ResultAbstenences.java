package com.jtcode.cuaderno;

import com.jtcode.cuaderno.POJO.Abstenence;
import java.io.Serializable;
import java.util.ArrayList;

public class ResultAbstenences implements Serializable{
        boolean code;
        int status;
        String message;
        ArrayList<Abstenence> abstenences;
        int last;

        public boolean getCode() {
            return code;
        }

        public void setCode(boolean code) {
            this.code = code;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Abstenence> getAbstenences() {
            return abstenences;
        }

        public int getLast() {
            return last;
        }

        public void setLast(int last) {
            this.last = last; }
    }